class Hg
  # Clone the repository
  def Hg.clone(url, dst)
    return Report.run("hg clone #{url} #{dst}")
  end
  # Get data from the repository, and update to latest revision
  def Hg.fetch(dst)
    return Report.run("cd #{dst}; hg pull; hg update")
  end
  # Get tags from the repository
  def Hg.tags(dst)
    tags = `cd #{dst}; hg tags`
    t = []
    tags.split("\n").each() { |l|
      t << /([^ ]+).*$/.match(l)[1]
    }
    return t
  end
  def Hg.cat(dst, filename, version)
    return `cd #{dst}; hg cat -r #{version} #{filename}`
  end
  def Hg.archive(repo, dst, dir, version)
    Report.run("cd #{repo}; hg archive -r #{version} -p #{dir} -t tbz2 #{dst}")
  end
end

class Git
  # Clone the repository
  def Git.clone(url, dst)
    return Report.run("git clone #{url} #{dst}")
  end
  # Get data from the repository, and update to latest revision
  def Git.fetch(dst)
    return Report.run("cd #{dst}; git pull")
  end
end
