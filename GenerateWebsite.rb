#!/usr/bin/ruby

$LOAD_PATH << File.dirname( __FILE__  )

templatePath = File.dirname( __FILE__  ) + "/templates/"

require 'Report'
require 'Extension'
require 'VersionControl'
require 'Configuration'

if (not File.directory?(Configuration::WebsiteDir))
  Dir.mkdir(Configuration::WebsiteDir)
end

downloadsdir = Configuration::WebsiteDir + "/downloads/"

if (not File.directory?(downloadsdir))
  Dir.mkdir(downloadsdir)
end

# Load templates
Extensionitem_template = File.open(templatePath + "extensionitem.html", "r").readlines.join("\n")
Extensionpage_template = File.open(templatePath + "extensionpage.html", "r").readlines.join("\n")
Extensionscreenshot_template = File.open(templatePath + "extensionscreenshot.html", "r").readlines.join("\n")
Extensiondownload_template = File.open(templatePath + "extensiondownload.html", "r").readlines.join("\n")
Index_template = File.open(templatePath + "index.html", "r").readlines.join("\n")


# Generate items
extensionitems = ""

Configuration::Extensions.each() { |extension|
  metadata = extension.metadata
  if(metadata)
    page_url = extension.name + ".html"
    # Add to the list
    item = Extensionitem_template.clone
    item.sub!("$EXTENSION_PAGE_LINK$", page_url)
    item.sub!("$EXTENSION_NAME$", metadata['Name'])
    item.sub!("$EXTENSION_SHORT_DESCRIPTION$", metadata['ShortDescription'])
    
    extensionitems += item
    
    # Create the extension page
    page = Extensionpage_template.clone
    page.gsub!("$EXTENSION_NAME$", metadata['Name'])
    page.sub!("$EXTENSION_DESCRIPTION$", metadata['LongDescription'])
    
    # Create the screenshots section
    screenshotsitem = metadata["Screenshots"]
    if(screenshotsitem.nil?)
      screenshotssection = "<p><i>No screenshots.</i></p>"
    else
      screenshotssection = ""
      screenshotsitem.split(";").each() { |x|
        sitem = Extensionscreenshot_template.clone
        sitem.sub!("$THUMBNAIL$", x + ".th.jpg" )
        sitem.sub!("$FULLSIZE$", x )
        screenshotssection += sitem
        `convert -thumbnail 200x200 #{File.dirname( __FILE__ )}/#{x} #{Configuration::WebsiteDir}/#{x}.th.jpg`
      }
    end
    page.sub!("$EXTENSION_SCREENSHOTS$", screenshotssection)
    
    # Create the download page
    versions = extension.versions
    if(versions.empty?)
      downloadsection = "<p><i>No downloads.</i></p>"
    else
      downloadsection = "<ul>"
      
      versions.each() { |v|
        ditem = Extensiondownload_template.clone
        ditem.sub!("$EXTENSION_VERSION$", v )
        ditem.sub!("$ARCHIVE$", "downloads/#{extension.name}-#{v}.tar.bz2" )
        ditem.sub!("$KRITA_VERSION$", extension.kritaversion(v) )
        
        extension.archive(v, "#{extension.name}-#{v}", downloadsdir + "/#{extension.name}-#{v}.tar.bz2")
        
        downloadsection += ditem
      }
      
      downloadsection += "<ul>"
    end
    page.sub!("$EXTENSION_DOWNLOAD$", downloadsection)
    
    # Save the extension page
    File.open(Configuration::WebsiteDir + "/" + page_url, 'w') {|f| f.write(page) }    
    
  end
}

index = Index_template.clone
index.sub!( "$EXTENSIONS_LIST$", extensionitems )

File.open(Configuration::WebsiteDir + "/index.html", 'w') {|f| f.write(index) }

`rsync -av #{File.dirname( __FILE__ )}/media #{Configuration::WebsiteDir}`
`rsync -av #{File.dirname( __FILE__ )}/screenshots #{Configuration::WebsiteDir}`

# Do rsync ?
if (Configuration::RsyncWebsite)
  puts "Synchronization with #{Configuration::RsyncWebsiteDst}"
  puts `rsync -av --rsh=ssh #{Configuration::WebsiteDir}/* #{Configuration::RsyncWebsiteDst}`
end
