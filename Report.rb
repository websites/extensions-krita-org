class Report
  # status = true means success, = false means failure
  attr_reader :status, :name, :text
  def initialize(name)
    @name = name
    @text = ""
    @status = true
  end
  # Change the status of the report, if the current status is already false, no change is made
  def status=(st)
    @status = false if(st == false)
  end
  def appendText(txt)
    @text += "\n"
    @text += txt
  end
  # merge with the other report
  def merge(r)
    appendText(r.name)
    appendText(r.text)
    self.status = r.status
  end
  def Report.run(command)
    report = Report.new(command)
    report.appendText(`#{command}`)
    report.status = ($?.exitstatus == 0)
    return report
  end
end
