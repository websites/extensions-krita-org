#
# Control how to build, and package an application
#
class Extension
  attr_reader :name, :website
  def initialize(repo, name, versioncontrol, website)
    @repo = repo
    @name = name
    @versioncontrol = versioncontrol
    @website = website
  end
  def compile
    report = Report.new(@name)
    
    # Get the source code
    if(File.directory?(srcDir))
      report.merge( @versioncontrol.fetch( srcDir) )
    else
      report.merge( @versioncontrol.clone( @repo, srcDir) )
    end
    return report unless report.status
    
    # Remove the build directory, if desirable
    if (Configuration::CleanBuild and File.exist?(buildDir))
      report.merge( Report.run("rm -rf #{buildDir}") )
    end
    
    # Create the build dir
    if(File.exists?(buildDir) and not File.directory?(buildDir))
      report.appendText("#{buildDir} is not a directory")
      report.status = false
      return report
    elsif(not File.exists?(buildDir))
      Dir.mkdir(buildDir)
    end
    
    # Run cmake
    report.merge( Report.run("cd #{buildDir}; cmake #{srcDir} -DCMAKE_INSTALL_PREFIX=#{Configuration::InstDir} 2>&1") )
    
    # Run make
    report.merge( Report.run("cd #{buildDir}; make -j3 2>&1") )
    
    if(Configuration::InstallExtension)
      report.merge( Report.run("cd #{buildDir}; make install 2>&1") )
    end
    
    return report
  end
  def metadata
    # Read metadata
    filename = srcDir + "/DESCRIPTION"
    return nil unless(File.exists?(filename))
    m = {}
    begin
      file = File.new(filename, "r")
      r = /([^:]*):(.*)/
      longDescriptionMode = false
      longDescription = nil
      while (line = file.gets)
        if(longDescriptionMode)
          longDescription += line
        else
          a = r.match(line)
          if(a[1] == "LongDescription")
            longDescriptionMode = true
            longDescription = ""
          else
            m[ a[1] ] = a[2].strip
          end
        end
      end
      unless(longDescription.nil?)
        m[ "LongDescription" ] = longDescription
      end
      file.close
    rescue => err
      puts "Exception: #{err}"
      return nil
    end
    return m
  end
  def versions
    v = []
    @versioncontrol.tags( srcDir ).each() { |t|
      unless(/v[0-9]+\.[0-9]+\.[0-9]+/.match(t).nil?)
        v << t[1,t.size-1]
      end
    }
    return v
  end
  def kritaversion(v)
    description = @versioncontrol.cat(srcDir, "DESCRIPTION", "v" + v)
    v = /KritaVersion:(.*)/.match(description)[1]
    return v.lstrip
  end
  def archive(v, dirname, dst)
    unless(File.exist?(dst))
      @versioncontrol.archive(srcDir, dst, dirname, "v" + v)
    end
  end
  private
  def srcDir
    return Configuration::SrcDir + @name
  end
  def buildDir
    return Configuration::BuildDir + @name
  end
end
