#!/usr/bin/ruby

$LOAD_PATH << File.dirname( __FILE__  )

require 'Report'
require 'Extension'
require 'VersionControl'
require 'Configuration'

reporthome = <<END
<html>
  <head>
    <title>Krita Extensions Build Report</title>
  </head>
<body>
  <h1>Compilation results</h1>
  <p>Date: #{Time.now}</p>
END

if (Configuration::WriteReport and not File.directory?(Configuration::ReportDir))
  Dir.mkdir(Configuration::ReportDir)
end

Configuration::Extensions.each() { |extension|
  puts "Test compilation of: #{extension.name}"
  report = extension.compile
  
  if (Configuration::WriteReport)
    reporthome += "<p><b>#{report.name}:</b> "
    if(report.status)
      reporthome += "<span style='color:green'>success</span>"
    else
      reporthome += "<span style='color:red'>failed</span>"
    end
    buildlog = "#{report.name}.txt"
    reporthome += " <a href='#{buildlog}'>download build log</a> <a href='#{extension.website}' target='_blank'>#{report.name} website</a></p>"
    f = File.new(Configuration::ReportDir + buildlog, "w")
    f.puts(report.text)
    f.close
  else
    puts "Report for: #{report.name}"
    puts report.text
    if(report.status)
      puts "Compilation of #{report.name} was successfull"
    else
      puts "Compilation of #{report.name} failed."
    end
  end
}

reporthome += "</body"

# Write the report
if (Configuration::WriteReport)
  f = File.new(Configuration::ReportDir + "index.html", "w")
  f.puts(reporthome)
  f.close
end

# Do rsync ?
if (Configuration::RsyncReport)
  puts "Synchronization with #{Configuration::RsyncReportDst}"
  puts `rsync -av --rsh=ssh report/* #{Configuration::RsyncReportDst}`
end
