begin
require 'LocalConfiguration'
rescue Exception
end

# Variables
module Configuration
  # Define which method to use to access
  unless(const_defined?("GitoriousVersionControl"))
    GitoriousVersionControl = Git
  end
  # List of extensions
  Extensions = [
      Extension.new("git://gitorious.org/krita-extensions/humanbody.git", "HumanBody", GitoriousVersionControl, "http://gitorious.org/krita-extensions/humanbody"),
      Extension.new("git://gitorious.org/krita-extensions/dither.git", "Dither", GitoriousVersionControl, "http://gitorious.org/krita-extensions/dither"),
      Extension.new("git://gitorious.org/krita-extensions/ctlbrush.git", "CTLBrush", GitoriousVersionControl, "http://gitorious.org/krita-extensions/ctlbrush"),
      Extension.new("git://gitorious.org/krita-extensions/cimg.git", "CImg", GitoriousVersionControl, "http://gitorious.org/krita-extensions/cimg"),
      Extension.new("git://gitorious.org/krita-extensions/analogies.git", "Analogies", GitoriousVersionControl, "http://gitorious.org/krita-extensions/analogies"),
      Extension.new("git://gitorious.org/krita-extensions/imagecomplete.git", "ImageComplete", GitoriousVersionControl, "http://gitorious.org/krita-extensions/imagecomplete"),
      Extension.new("git://gitorious.org/krita-extensions/deskew.git", "Deskew", GitoriousVersionControl, "http://gitorious.org/krita-extensions/deskew"),
      Extension.new("git://gitorious.org/krita-extensions/grayscalizer.git", "Grayscalizer", GitoriousVersionControl, "http://gitorious.org/krita-extensions/grayscalizer"),
      Extension.new("git://gitorious.org/krita-extensions/linesampler.git", "LineSampler", GitoriousVersionControl, "http://gitorious.org/krita-extensions/linesampler"),
      Extension.new("git://gitorious.org/krita-extensions/pyramidalsharpening.git", "PyramidalSharpening", GitoriousVersionControl, "http://gitorious.org/krita-extensions/pyramidalsharpening")
      ]

  # Define whether the build directory should be removed before new build
  unless(const_defined?("CleanBuild"))
    CleanBuild = false
  end

  # Define whether the reports are saved on disk
  unless(const_defined?("WriteReport"))
    WriteReport = true
  end

  # Define wether to install extension
  unless(const_defined?("InstallExtension"))
    InstallExtension = false
  end
  
  # Define wether to rsync the report, if you do set it to true, you need to define RsyncDst
  unless(const_defined?("RsyncReport"))
    RsyncReport = false
  end
  
  # Define wether to rsync the report, if you do set it to true, you need to define RsyncDst
  unless(const_defined?("RsyncWebsite"))
    RsyncWebsite = false
  end

  # Define where the source code and build are to be made
  BuildDir = ENV["PWD"] + "/build/"
  SrcDir = ENV["PWD"] + "/src/"
  
  # Define the install directory
  unless(const_defined?("InstDir"))
    InstDir = ENV["PWD"] + "/inst/"
  end
  ReportDir = ENV["PWD"] + "/report/"
  WebsiteDir = ENV["PWD"] + "/website/"
end
